package Lab_12;

/*NOMBRES DE LOS INTEGRANTES DEL GRUPO:
    -SAMUEL PEREZ
    -RODRIGO JARA

    RESPUESTA A LA PREGUNA:
    Explique lo que modificaría de la implementación para que la estructura fuera max heap en
    vez de min heap.

    R/: Primero que todo revisariamos como esta hecho el metodo de insercion y veriamos que metodo usa para poder formar el heap,
    al percatarnos de su construccion, nos fijamos en el metodo BinomialHeapUnion (Linea 44 de este codigo) donde en un inicio realiza una insercion normal hasta que se llega a la linea numero
    55 de este codigo donde se realiza la siguiente evaluacion  "if(x.key <= nextx.key)" donde en resumidas cuentas realiza una comparacion que revisa el valor del dato que esta dentro del nodo
    padre guardado en la variable "x" con su hermano guardado en la variable "nextx" verificando cual de los dos es de menor valor para asi convertir al hermano en el nuevo padre y ir agregando desde ahi.
    Entonces lo que realizamos fue simplemente el cambio de simbolo comparativo de un "<" a un ">" quedando la comparacion de la manera : if(x.key >= nextx.key) donde ahora se fijara en que valor de los dos
    es mayor para asi continuar el proceso de insercion creandose a partir de ese cambio un MAX BINOMIAL HEAP.

*/
public class ColaBinomial {
    NodoCB head; // Puntero al primer árbol binomial de la cola
    ColaBinomial(){head=null;} // Constructor



    //==================================================
    //Inserción de un elemento en la cola binomial
    //==================================================
    void Insert(int a) {
        ColaBinomial H1 = new ColaBinomial();
        H1.head = new NodoCB(null, null, null, a, (short)0);
        head = BinomialHeapUnion(H1);
    }


    public void Print() {
        System.out.println("Cola Binomial:");
        if (head != null) {
            head.print(0);
        }
    }


    //====================================================
    // Método privados
    //====================================================

    //====================================================
    //Unión (fusión de dos colas binomiales)
    //====================================================
    private NodoCB BinomialHeapUnion(ColaBinomial H2){
        ColaBinomial H = BinomialHeapMerge(this, H2);
        if(H.head==null) return H.head;
        NodoCB prevx= null;
        NodoCB x = H.head;
        NodoCB nextx=x.sibling;
        while(nextx!=null) {
            if ((x.degree != nextx.degree) ||  (nextx.sibling !=null && nextx.sibling.degree == x.degree)){
                prevx = x;
                x = nextx;
            }
            else if(x.key >= nextx.key) {
                x.sibling= nextx.sibling;
                nextx.BinomialLink(x);
            }
            else {
                if(prevx==null) H.head=nextx;
                else  prevx.sibling= nextx;
                x.BinomialLink(nextx);
                x = nextx;
            }
            nextx = x.sibling;
        }
        return H.head;
    }

    //===========================================================
    // Mezcla de dos colas Binomiales. A partir de dos colas
    // se obtine una tercera que contiene los árboles binomiales
    // de las dos colas ordenados por k
    //===========================================================
    private  ColaBinomial BinomialHeapMerge(ColaBinomial H1, ColaBinomial H2) {
        NodoCB h1 = H1.head;
        NodoCB h2 = H2.head;
        if (h1==null) return H2;
        if (h2==null) return H1;
        ColaBinomial H = new ColaBinomial();
        NodoCB ini = null;
        NodoCB fin= null;
        while(h1!=null && h2!=null) {
            if(h1.degree <= h2.degree) {
                if(ini == null) {ini = fin= h1;}
                else {fin.sibling = h1; fin = h1;}
                h1 = h1.sibling;
            }
            else  {
                if(ini == null) {ini = fin= h2;}
                else {fin.sibling = h2; fin = h2;}
                h2 = h2.sibling;
            }
        }
        if(h1==null) fin.sibling = h2;
        if(h2==null) fin.sibling = h1;
        H.head=ini;
        return  H;



    }

    public int maxBinomialTree() {
        if (head == null) {
            return 0;
        } else {
            NodoCB helper = head;
            int max = helper.key;
            while (helper != null) {
                if (helper.key > max) {
                    max = helper.key;
                }
                helper = helper.sibling;
            }
            return max;
        }
    }

}

