package Lab_6;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Stack<ArbolBinario> nodos=new Stack<>();
        for (int i = 0; i < 10; i++) {
            nodos.add(new ArbolBinario(i));
        }
        //voy emparejando de a dos nodos para construir el árbol
        while(nodos.size()>1){
            ArbolBinario iz=nodos.pop();
            ArbolBinario der=nodos.pop();
            nodos.push(new ArbolBinario(iz.dato+der.dato,iz,der));
        }
        ArbolBinario raiz=nodos.pop();
        System.out.println("Altura del arbol: "+raiz.altura());
        System.out.println("Tamaño del arbol (total de nodos): "+ raiz.size());
        raiz.tree();

        ArbolBinario[] nodos2=new ArbolBinario[32];
        for (int i = 0; i < nodos2.length; i++) {
            nodos2[i]=new ArbolBinario(i);
        }
        ArbolBinario raiz2=generaBinario(nodos2, 0, 31);
        System.out.println("Altura del arbol: "+raiz2.altura());
        System.out.println("Tamaño del arbol (total de nodos): "+ raiz2.size());
        raiz2.tree();
        System.out.println("iNGRESE ALTURA:");
        Scanner sc = new Scanner ( System.in);
        int n = sc.nextInt();

        System.out.println("El valor de los nodos de la altura: "+ n+ " del arbol 1 es:" );
        raiz.nodosAltura(n);
        System.out.println("");
        System.out.println("El valor de los nodos de la altura: "+ n+ " del arbol 2 es:" );
        raiz2.nodosAltura(n);

    }

    public static ArbolBinario generaBinario(ArbolBinario[] nodos, int i, int j){
        if(i>j){
            //caso base
            return null;
        }
        int mitad=(i+j)/2;
        nodos[mitad].iz=generaBinario(nodos, i,mitad-1);
        nodos[mitad].der=generaBinario(nodos,mitad+1,j);
        return nodos[mitad];
    }


}

