package Lab_1;

public class TimeDate {
    private int fecha;

    public TimeDate(){
        fecha=0;
    }

    public int getAño(){
        int año = fecha >>> 20;
        return año;
    }

    public int getMes(){
        int mes = (fecha << 12) >>> 28;
        return mes;
    }

    public int getDia(){
        int dia = (fecha << 16) >>> 27;
        return dia;
    }

    public int getHora(){
        int hora = (fecha << 21) >>> 27;
        return  hora;
    }

    public int getMinuto(){
        int minuto = (fecha << 27) >>> 26;
        return minuto;
    }

    public void setAño(int año){
        if(año>=0 && año<4096){
            int mask=8191;//00000000000011111111111111111111
            fecha = (fecha & mask) | (año << 20);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Año no reconocible");
        }
    }

    public void setMes(int mes){
        if(mes>=0 && mes<13){
            int mask=2146500607 | (1<<31);//11111111111100001111111111111111
            fecha = (fecha & mask) | (mes << 16);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Mes no valido");
        }
    }

    public void setDia(int dia){
        if(dia>=0 && dia<32){
            int mask =2147420159 | (1<<31);//11111111111111110000011111111111
            fecha = (fecha & mask) | (dia << 11);
            //System.out.println(Long.toBinaryString(fecha));
        }else{
            System.out.println("Dia no valido");
        }

    }

    public void setHora(int hora){
        if (hora>0 && hora < 24){
            int mask = 2147481663 | ( 1<< 31);
            fecha = (fecha & mask) | (hora << 6 );
        }else{
            System.out.println("Hora invalida");
        }
    }

    public void setMinuto (int minuto ){
        if(minuto> 0 && minuto < 60 ){
            int mask = 2147483584 | (1 << 31);
            fecha = (fecha & mask) | (minuto);
        }
    }

    @Override
    public String toString() {
        if(getMes()<10){
            return getDia() + "/0" + getMes() + "/" + getAño();
        }else{
            return getDia() + "/" + getMes() + "/" + getAño();
        }
    }
    //.
}
