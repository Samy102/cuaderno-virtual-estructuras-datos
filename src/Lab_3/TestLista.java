package Lab_3;

import java.util.Scanner;

public class TestLista {
    public static void main(String [] args) {
        Lista l = new Lista();
        Scanner sc = new Scanner(System.in);




        System.out.println("Ingrese cantidad de Datos a incorporar a la lista");
        int num = sc.nextInt();
        System.out.println("Ingrese los datos que ingresaran a la lista. ");

        for (int i = 0; i < num; i++) {
            System.out.print("\nDatos " +i+ ": ");
            int data = sc.nextInt();
            l.InsertaInicio(data);
        }

        System.out.println("La lista tiene los siguientes datos:");
        l.Print();
        System.out.println("");


        System.out.println("El promedio de la lista con respecto a los mayores es:");
        System.out.println();
        System.out.println("Y sus eleentos mayores al promedio ordenados son:");
        System.out.println();
        System.out.println("Vacia?: " + l.EstaVacia());
        System.out.println("\nSize: " + l.Size());
        l.InsertaInicio(12);
        System.out.println("Vacia?: " + l.EstaVacia());
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.InsertaInicio(32);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.InsertaFinal(51);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(12);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(32);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(51);
        System.out.println("\nSize: " + l.Size());
        System.out.println("Vacia?: " + l.EstaVacia());

    }
}

