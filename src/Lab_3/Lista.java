package Lab_3;

public class Lista {
    private Nodo laCabeza;
    Lista() {
        laCabeza = null;
    }
    //---Inserta un objeto(int) al comienzo de la lista
    public void InsertaInicio(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else  laCabeza = new Nodo(o, laCabeza);
    }

    //---- Inserta al final ----
    public void InsertaFinal(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else{
            Nodo t;
            for(t = laCabeza; t.next != null; t= t.next) ;;
            t.next = new Nodo(o,null);
        }
    }

    // ---cuenta la cantidad de nodos de la lista (Size)
    public int Size() {
        int tnodos=0;
        for(Nodo t = laCabeza; t !=null; t= t.next)  tnodos++;
        return tnodos;
    }

    //eliminar un nodo de la lista
// Elimina el primer nodo n tal que o.elObjeto==o
    public void Eliminar(int o) {
        if(!EstaVacia()) {
            if(laCabeza.elObjeto==o) laCabeza = laCabeza.next;
            else {
                Nodo p = laCabeza;
                Nodo t = laCabeza.next;
                while (t !=null && t.elObjeto != o)  {
                    p = t ; t = t.next;
                }
                if(t.elObjeto==o) p.next = t.next;
            }
        }
    }

    // Verifica si la lista está vacias;
    public boolean EstaVacia() {
        return laCabeza == null;
    }

    //-----Imprime la lista-----
    void Print() {
        if(laCabeza!=null) Imprimir(laCabeza);
        else System.out.println("Lista Vacia");
    }

    void Imprimir(Nodo m ) {
        if(m !=null) {m.Print(); Imprimir(m.next);}
    }

    //-----Clase Nodo---------
    private class Nodo {
        public int elObjeto;
        public Nodo next;
        public Nodo(int nuevoObjeto, Nodo next)
        {this.elObjeto=nuevoObjeto; this.next = next;}
        void Print(){ System.out.print("- " + elObjeto);}
    }

    public Lista GetMayoresProm(int promedio){
        Lista prom = new Lista();
        Nodo x = laCabeza;
        while(x!=null){
            if (x.elObjeto>promedio){
                prom.InsertaInicio(x.elObjeto);
            }
            x=x.next;
        }

        return prom;
    }

    public int Promedio(Lista l) {
        Nodo n = l.laCabeza;
        int cant = 0, tam = Size();
        while (n != null) {
            cant += n.elObjeto;
            n = n.next;
        }
        if (tam == 0) {
            return 0;
        } else {
            return cant/tam;
        }

    }

    public Lista ListaOrdenada (Lista l){
        Lista Ordenada = new Lista();
        Nodo n = l.laCabeza;
        int data=0;
        while (n != null){
            if (n.elObjeto>data){
                data=n.elObjeto;
                Ordenada.InsertaInicio(n.elObjeto);
            }
            n=n.next;
        }
        return Ordenada;
    }

    public void InsertaOrden (int o ){
        if(EstaVacia()){
            laCabeza = new Nodo(o, null);
        }else if (laCabeza.elObjeto>o){
            InsertaInicio(o);
        }else{
            Nodo x = laCabeza;
            Nodo y = new Nodo(o, null);
            while (x.next!=null){
                if (x.next.elObjeto>y.elObjeto){
                    y.next = x.next;
                    x.next= y;
                    return;
                }
                x=x.next;
            }
            if (x.next==null){
                x.next=y;
            }
        }
    }
}

