package Lab_4;

import java.util.LinkedList;
import java.util.Queue;

//EJERCICIO FILA
// 933. Número de llamadas recientes

public class Solucion_Fila {

    Queue<Integer> queue;
    public void RecentCounter() {
        queue = new LinkedList<>();
    }

    public int ping(int t) {
        while(queue.peek()!=null&&t - queue.peek() > 3000)
            queue.poll();
        queue.add(t);
        return queue.size();
    }


}

