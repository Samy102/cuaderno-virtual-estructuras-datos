package Lab_4;

public class Ejercicio_LeetCode {

    //1290. Convertir número binario en una lista enlazada a entero


    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }



    public int getDecimalValue(ListNode head) {
        ListNode n = head;
        int number = 0;
        while (n !=null ){
            number = (number<< 1) | n.val;
            n=n.next;
        }

        return number;
    }
}

